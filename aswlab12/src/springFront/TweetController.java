package springFront;

import java.util.HashSet;
import java.util.Vector;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import models.ModelException;
import models.Tweet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;


@Controller
@SessionAttributes("likedTable")
public class TweetController {

	private static final Logger logger = LoggerFactory.getLogger(TweetController.class);
	
	@ModelAttribute("likedTable")
	public HashSet<Integer> newLikedTable() {
		return new HashSet<Integer>();
	}

	@RequestMapping(value = "/like", method = RequestMethod.GET)
	public  @ResponseBody String like(@RequestParam("tweetid") Integer tweetID, HttpSession session, 
			@ModelAttribute("likedTable") HashSet<Integer> table) {

		logger.info("Liking Tweet with id = "+tweetID) ;

		String result = "";
		try {
			
			Tweet tweet = Tweet.findById(tweetID);
			int likes = tweet.getLikes();
			if (!table.contains(tweetID)) {
				likes = likes +1;
				tweet.setLikes(likes);
				tweet.update();
				table.add(tweetID);
			}			
			result = String.valueOf(likes);
			logger.info("Tweet #"+tweetID+"'s likes = "+result) ;
		}
		catch (ModelException ex) {
			logger.info("Tweet inc error: " + ex.getMessageList().get(0)) ;
			session.setAttribute("theList",ex.getMessageList());
			result = "<a href=error.sp>ERROR</a>";
		}
		return result;
	}


	@RequestMapping(value = "/tweet", method = RequestMethod.POST)
	public  String tweet(@Valid @ModelAttribute("tweetForm") TweetForm tweetForm, BindingResult binding, 
			HttpSession session, Model model) {

		logger.info("Adding a new tweet") ;

		if (binding.hasErrors()) {
			return "browser";
		}

		Integer userID = (Integer) session.getAttribute("loggedUserID");
		String content = tweetForm.getTweet_content();
		
		Vector<String> validationErrorList = new Vector<String>();
		Boolean paramsOK = true;

		if (userID == null) { validationErrorList.addElement("You must login first"); paramsOK = false; }

		if (paramsOK)
		{
			try {
				Tweet.create(userID, content);
				logger.info("The new tweet has the following content: "+content) ;
				return ("redirect:/wall");
			}
			catch (ModelException ex) {
				model.addAttribute("theList",ex.getMessageList());
				return ("error");
			}
		}
		else {
			model.addAttribute("theList",validationErrorList);
			return ("error");
		}
	}
}
