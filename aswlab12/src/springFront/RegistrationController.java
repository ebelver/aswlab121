package springFront;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import models.ModelException;
import models.User;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping("/registration")
public class RegistrationController {

	private static final Logger logger = LoggerFactory.getLogger(RegistrationController.class);

	@InitBinder
	protected void initBinder(HttpServletRequest request, WebDataBinder binder) {
		binder.setValidator(new RegFormValidator(request));
	}

	@RequestMapping(method = RequestMethod.GET)
	public String setupForm(Model model) {
		return "user_registration";
	}

	@ModelAttribute("registrationForm")
	public RegistrationForm newRegistrationForm() {
		RegistrationForm regForm = new RegistrationForm();
		return regForm;
	}

	@RequestMapping(method = RequestMethod.POST)
	public  String register(@Valid @ModelAttribute("registrationForm") RegistrationForm regForm, 
			BindingResult binding, HttpSession session, Model model) 
	{
		if (binding.hasErrors()) return "user_registration";

		String username = regForm.getReg_username();
		String password = regForm.getReg_password();

		logger.info("Registring new  user with username = "+username);

		try {
			User new_user = User.create(username, password);
			session.setAttribute("loggedUserNAME",username);
			session.setAttribute("loggedUserID",new_user.getUser_id());
			return ("redirect:/wall");
		}
		catch (ModelException ex) {
			if (ex.getMessageList().elementAt(0).startsWith("Username")){
				String mess = "There is another user with &lsquo;{0}&rsquo; as username";
				binding.rejectValue("reg_username", "username.duplicated", new String[] {username}, mess);
				return "user_registration";
			}
			else {
				model.addAttribute("theList",ex.getMessageList());
				return ("error");
			}
		}
	}
}
